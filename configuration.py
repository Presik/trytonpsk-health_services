from trytond.model import fields
from trytond.pool import PoolMeta


class HealthConfiguration(metaclass=PoolMeta):
    "Health Configuration"
    __name__ = "health.configuration"

    health_service_sequence = fields.Many2One('ir.sequence',
        'Health Service Sequence')
