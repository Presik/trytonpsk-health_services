# This file is part of the sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.model import fields, ModelView, ModelSQL, Workflow
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.exceptions import UserError

_ZERO = Decimal("0.0")

PAYMENT_CODES = [
    ('', ''),
    ('1', 'Instrumento no definido'),
    ('10', 'Efectivo'),
    ('44', 'Nota Cambiaria'),
    ('20', 'Cheque'),
    ('48', 'Tarjeta Crédito'),
    ('49', 'Tarjeta Débito'),
    ('42', 'Consignación bancaria'),
    ('47', 'Transferencia Débito Bancaria'),
    ('45', 'Transferencia Crédito Bancaria'),
]


class Journal(metaclass=PoolMeta):
    __name__ = 'account.statement.journal'
    devices = fields.One2Many('payment.device', 'journal', 'Devices')
    kind = fields.Selection([
            ('cash', 'Cash'),
            ('electronic', 'Electronic'),
            ('transfer', 'Transfer'),
            ('payment', 'Payment'),
            ('other', 'Other'),
        ], 'Kind', select=True)
    default_start_balance = fields.Numeric('Default Start Balance by Device')
    require_voucher = fields.Boolean('Require Voucher')
    party = fields.Many2One('party.party', 'Party',
        states={'invisible': ~Eval('require_party')})

    @staticmethod
    def default_kind():
        return 'cash'


class Statement(metaclass=PoolMeta):
    __name__ = 'account.statement'
    turn = fields.Integer('Turn')
    count_money = fields.One2Many(
        'sale_pos.money_count', 'statement', 'Count Money')
    payment_device = fields.Many2One('payment.device', 'Device', required=False)
    total_money = fields.Function(
        fields.Numeric("Total Money"), 'get_total_money')
    amount_difference = fields.Function(fields.Numeric("Amount Difference"),
        'get_amount_difference')

    @classmethod
    def __setup__(cls):
        super(Statement, cls).__setup__()

    @classmethod
    def cron_autopost(cls):
        # Add automatic post for validate statement
        statements = cls.search([
            ('state', '=', 'validated'),
        ], order=[('date', 'ASC')])
        if statements:
            cls.post([statements[0]])

    def get_total_money(self, name):
        res = 0
        for line in self.count_money:
            res = res + line.amount
        if res:
            return res

    def get_amount_difference(self, name):
        res = 0
        amount = sum(l.amount for l in self.lines)
        if self.total_money is not None:
            res = self.total_money - amount
            return res

    @classmethod
    def create_move(cls, statements):
        '''Create move for the statements and try to reconcile the lines.
        Returns the list of move, statement and lines
        '''
        pool = Pool()
        Line = pool.get('account.statement.line')
        Move = pool.get('account.move')
        Invoice = pool.get('account.invoice')

        to_write = []
        moves = []
        for statement in statements:
            for line in statement.lines:
                if line.move:
                    continue
                if line.invoice and line.invoice.state in (
                        'draft', 'validated'):
                    if not line.invoice.invoice_date:
                        for sale in line.invoice.sales:
                            line.invoice.write(
                                [line.invoice], {
                                    'invoice_date': sale.sale_date}
                            )
                    Invoice.post([line.invoice])
                move = line._get_move2()
                Move.post([move])
                to_write.append([line])
                to_write.append({'move': move.id})

                moves.append(move)
                for mline in move.lines:
                    if mline.account == line.account:
                        line.reconcile([(mline, line)])
        if to_write:
            Line.write(*to_write)
        return moves

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate_statement(cls, statements):
        for statement in statements:
            getattr(statement, 'validate_%s' % statement.validation)()

        cls.create_move(statements)
        cls.write(statements, {
            'state': 'validated',
        })

    @classmethod
    def delete(cls, statements):
        for statement in statements:
            if statement.lines:
                raise UserError(
                    gettext('sale_pos.msg_cant_delete_statement', s=statement.rec_name))
            else:
                super(Statement, cls).delete(statements)


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    health_service = fields.Many2One('health.service', 'Service',
        ondelete='RESTRICT')
    voucher = fields.Char('Voucher Number')

    def get_move_line2(self, values):
        'Return counterpart Move Line for the amount'
        move_id = values['move_id']
        journal_account = values.get('journal_account', False)
        account = values.get('account')
        amount = values.get('amount')
        credit = _ZERO
        debit = _ZERO
        if journal_account:
            if amount >= 0:
                debit = amount
            else:
                credit = abs(amount)
        else:
            if amount >= 0:
                credit = amount
            else:
                debit = abs(amount)

        if not account:
            raise UserError(gettext(
                'health_services.msg_account_statement_journal',
                s=self.journal.rec_name
            ))
        res = {
            'description': self.description,
            'debit': debit,
            'credit': credit,
            'account': account.id,
            'party': self.party.id if account.party_required else None,
            'move': move_id,
        }
        return res

    def _get_move2(self):
        # Return Move for the grouping key
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Period = pool.get('account.period')
        Journal = pool.get('account.statement.journal')

        company_id = self.statement.company.id
        period_id = Period.find(company_id, date=self.date)
        _move = {
            'period': period_id,
            'journal': self.statement.journal.journal.id,
            'date': self.date,
            'origin': str(self.statement),
            'company': company_id,
            'state': 'draft',
        }

        move, = Move.create([_move])

        move_line1 = self.get_move_line2({
            'move_id': move.id,
            'account': self.account,
            'amount': self.amount
        })

        move_line2 = self.get_move_line2({
            'account': self.statement.journal.account,
            'move_id': move.id,
            'amount': self.amount,
            'journal_account': True,
        })
        to_create = [move_line1, move_line2]

        journals = Journal.search([
            ('kind', '=', 'cash')
        ])
        journal_ids = [j.id for j in journals]
        MoveLine.create(to_create)
        return move

    @classmethod
    def post_move(cls, lines):
        super(StatementLine, cls).post_move(lines)
        for s in lines:
            if s.invoice and s.move and s.invoice.state != 'paid':
                invoice = s.invoice
                move = s.move
                payment_lines = [l for l in move.lines if l.account
                                 == invoice.account and l.party == invoice.party]
                invoice.add_payment_lines({invoice: payment_lines})

    def create_move(self):
        # Overwrite sale_pos > create_move
        move = self._get_move2()
        self.write([self], {'move': move.id})


class OpenStatementStart(ModelView):
    'Open Statement'
    __name__ = 'open.statement.start'
    device = party = fields.Many2One('payment.device', 'Device', required=True)


class OpenStatementDone(ModelView):
    'Open Statement'
    __name__ = 'open.statement.done'
    result = fields.Text('Result', readonly=True)


class OpenStatement(Wizard):
    'Open Statement'
    __name__ = 'open.statement'
    start = StateView('open.statement.start',
          'health_services.open_statement_start', [
              Button('Cancel', 'end', 'tryton-cancel'),
              Button('Ok', 'create_', 'tryton-ok', default=True),
    ])
    create_ = StateTransition()
    done = StateView('open.statement.done',
         'health_services.open_statement_done', [
             Button('Done', 'end', 'tryton-ok', default=True),
        ])

    @classmethod
    def __setup__(cls):
        super(OpenStatement, cls).__setup__()

    def default_done(self, fields):
        return {
            'result': self.result,
        }

    def transition_create_(self):
        pool = Pool()
        User = pool.get('res.user')
        Statement = pool.get('account.statement')
        CountMoney = pool.get('sale_pos.money_count')
        BillMoney = pool.get('sale_pos.bill_money')
        money = BillMoney.search([])
        user = Transaction().user
        user = User(user)
        device = self.start.device
        journals = [j.id for j in device.journals]
        statements = Statement.search([
                ('journal', 'in', journals),
                ('payment_device', '=', device.id),
            ], order=[
            ('date', 'ASC'),
        ])
        journals_of_draft_statements = [
            s.journal for s in statements if s.state == 'draft'
        ]

        vlist = []
        result = ''
        for journal in device.journals:
            statements_today = Statement.search([
                ('journal', '=', journal.id),
                ('date', '=', date.today()),
                ('payment_device', '=', device.id),
            ])
            turn = len(statements_today) + 1
            if journal not in journals_of_draft_statements:
                values = {
                    'name': '%s - %s' % (device.rec_name, journal.rec_name),
                    'journal': journal.id,
                    'company': user.company.id,
                    'start_balance': journal.default_start_balance or Decimal('0.0'),
                    'end_balance': Decimal('0.0'),
                    'turn': turn,
                    'payment_device': device.id,
                }
                vlist.append(values)
                result += gettext(
                    'health_services.msg_open_statement', s=(journal.rec_name,)
                )
            else:
                result += gettext(
                    'health_services.msg_statement_already_opened',
                    s=(journal.rec_name,)
                )
        created_sts = Statement.create(vlist)
        for st in created_sts:
            if st.journal.kind == 'cash':
                to_create = []
                for m in money:
                    to_create.append({
                        'bill': m.value,
                        'quantity': 0,
                        'statement': st.id,
                    })
                CountMoney.create(to_create)
        self.result = result
        return 'done'


class CloseStatementStart(ModelView):
    'Close Statement'
    __name__ = 'close.statement.start'
    device = party = fields.Many2One('payment.device', 'Device', required=True)


class CloseStatementDone(ModelView):
    'Close Statement'
    __name__ = 'close.statement.done'
    result = fields.Text('Result', readonly=True)


class CloseStatement(Wizard):
    'Close Statement'
    __name__ = 'close.statement'
    start = StateView('close.statement.start',
        'health_services.close_statement_start', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'validate', 'tryton-ok', default=True),
    ])
    validate = StateTransition()
    done = StateView('close.statement.done',
        'health_services.close_statement_done', [
            Button('Done', 'end', 'tryton-ok', default=True),
    ])

    @classmethod
    def __setup__(cls):
        super(CloseStatement, cls).__setup__()

    def default_done(self, fields):
        return {
            'result': self.result,
        }

    def transition_validate(self):
        pool = Pool()
        User = pool.get('res.user')
        Statement = pool.get('account.statement')

        user = Transaction().user
        user = User(user)
        device = self.start.device
        journals = [j.id for j in device.journals]
        draft_statements = {
            s.journal: s for s in Statement.search([
                    ('journal', 'in', journals),
                    ('payment_device', '=', device.id),
                ], order=[
                ('create_date', 'ASC'),
            ])}

        result = ''
        statements = []
        for journal in device.journals:
            statement = draft_statements.get(journal)
            if statement and statement.state == 'draft':
                end_balance = statement.start_balance
                for line in statement.lines:
                    end_balance += line.amount
                statement.end_balance = end_balance
                statement.save()
                statements.append(statement)
                result += gettext('health_services.msg_close_statement',
                                  s=(statement.rec_name,))
            elif statement:
                result += gettext('health_services.msg_statement_already_closed',
                                  s=(statement.rec_name,))
            else:
                result += gettext('health_services.msg_not_statement_found',
                                  s=(journal.rec_name,))
        if statements:
            Statement.validate_statement(statements)
        self.result = result
        return 'done'


class MoneyCount(ModelSQL, ModelView):
    'Money Count'
    __name__ = 'sale_pos.money_count'
    statement = fields.Many2One('account.statement', 'Statement',
        required=True)
    bill = fields.Integer('Bill', required=True)
    quantity = fields.Integer('Quantity')
    amount = fields.Function(fields.Numeric('Amount', digits=(16, 2)),
        'get_amount')

    @staticmethod
    def default_quantity():
        return 0

    def get_amount(self, name=None):
        res = Decimal(0)
        if self.quantity and self.bill:
            res = Decimal(self.quantity * self.bill)
        return res

    @classmethod
    def __setup__(cls):
        super(MoneyCount, cls).__setup__()
        cls._order.insert(0, ('bill', 'DESC'))


class BillMoney(ModelSQL, ModelView):
    'Bill Money'
    __name__ = 'sale_pos.bill_money'
    value = fields.Integer('Value', required=True)
