
from trytond.pool import Pool
from .wizard import wizard_health_services
from . import invoice
from . import services
from . import configuration
from . import statement
from . import device


def register():
    Pool.register(
        configuration.HealthConfiguration,
        services.HealthService,
        services.HealthServiceLine,
        services.PatientPrescriptionOrder,
        services.ServicePaymentForm,
        services.Appointment,
        invoice.Invoice,
        invoice.InvoiceLine,
        device.PaymentDevice,
        device.PaymentDeviceStatementJournal,
        statement.Journal,
        statement.Statement,
        statement.StatementLine,
        statement.OpenStatementStart,
        statement.OpenStatementDone,
        statement.CloseStatementStart,
        statement.CloseStatementDone,
        statement.BillMoney,
        statement.MoneyCount,
        wizard_health_services.CreateServiceInvoiceInit,
        module='health_services', type_='model')
    Pool.register(
        services.WizardServicePayment,
        wizard_health_services.CreateServiceInvoice,
        statement.OpenStatement,
        statement.CloseStatement,
        module='health_services', type_='wizard')
    Pool.register(
        services.PaymentReceipt,
        module='health_services', type_='report')
