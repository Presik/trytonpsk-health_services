from decimal import Decimal
from datetime import date, datetime, timedelta

from trytond.model import ModelView, ModelSQL, Workflow, fields, Unique
from trytond.pyson import Eval, Equal, Bool
from trytond.pool import Pool, PoolMeta
from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.wizard import (
    Wizard, StateTransition, StateView, Button, StateReport)
from trytond.transaction import Transaction
from trytond.modules.product import round_price
from trytond.report import Report

ZERO = Decimal('0.00')


class HealthService(Workflow, ModelSQL, ModelView):
    'Health Service'
    __name__ = 'health.service'

    STATES = {'readonly': Eval('state') == 'invoiced'}
    number = fields.Char('Number ID', readonly=True)
    desc = fields.Char('Description')
    patient = fields.Many2One('health.patient', 'Patient', required=True,
        states=STATES)
    institution = fields.Many2One('health.institution', 'Institution',
        states=STATES)
    service_date = fields.Date('Date')
    lines = fields.One2Many('health.service.line', 'service', 'Service Line',
        help="Service Lines")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('finished', 'Finished'),
        ('invoiced', 'Invoiced'),
        ('paid', 'Paid'),
        ], 'State', readonly=True)
    invoice_to = fields.Many2One('party.party', 'Invoice to')
    total_amount = fields.Function(fields.Numeric('Total Amount',
        digits=(16, 2)), 'get_total_amount')
    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': Eval('state') != 'draft'}, depends=['state'])
    insurance_plan = fields.Many2One('health.insurance.plan',
        'Insurance Plan', states=STATES)
    payments = fields.One2Many('account.statement.line', 'health_service',
        'Payments')
    paid_amount = fields.Function(fields.Numeric('Paid Amount', digits=(16, 2)),
        'get_paid_amount')
    payment_device = fields.Many2One('payment.device', 'Payment Device')
    discount_authorized = fields.Numeric('Discount Authorized', states=STATES)
    balance_payable = fields.Function(fields.Numeric('Balance Payable',
        digits=(16, 2), readonly=True), 'get_balance_payable')
    authorization_code = fields.Char('Authorization Code')
    authorized_by = fields.Char('Authorized By')

    @classmethod
    def __setup__(cls):
        super(HealthService, cls).__setup__()

        t = cls.__table__()
        cls._sql_constraints = [(
            'number_unique', Unique(t, t.number),
            'The Service Number ID must be unique'
        )]
        cls._transitions |= set((
            ('draft', 'finished'),
            ('finished', 'invoiced'),
            ('invoiced', 'paid'),
        ))
        cls._buttons.update({
            'button_set_to_draft': {
                'invisible': Equal(Eval('state'), 'draft')
            },
            'wizard_payment_form': {
                'invisible': Eval('state') != 'finished',
            },
            'finish': {
                'invisible': Eval('state') != 'draft',
            }
        })
        cls._order.insert(0, ('number', 'DESC'))
        cls._order.insert(1, ('service_date', 'DESC'))

    @staticmethod
    def default_service_date():
        return date.today()

    def get_rec_name(self, name):
        res = ''
        if self.number:
            res = f'{self.number} - ' + self.patient.rec_name
        return res

    @classmethod
    def search_rec_name(cls, name, clause):
        _, operator, value = clause
        if operator.startswith('!') or operator.startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [
            bool_op,
            ('patient', operator, value),
            ('number', operator, value),
            ('desc', operator, value),
        ]
        return domain

    def get_balance_payable(self, name):
        if self.total_amount:
            return self.total_amount - self.paid_amount

    def get_total_amount(self, name):
        res = sum([
            l.unit_price * l.qty for l in self.lines if l.unit_price and l.qty
        ])
        return res

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.appointment']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @classmethod
    @ModelView.button
    def button_set_to_draft(cls, records):
        cls.write(records, {'state': 'draft'})

    # @classmethod
    # @ModelView.button_action('health_services.wizard_service_invoice_payment')
    # def wizard_service_payment(cls, records):
    #     pass

    @classmethod
    @ModelView.button_action('health_services.wizard_payment_form')
    def wizard_payment_form(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, records):
        pass

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()
        if not config.health_service_sequence:
            raise UserError(
                gettext('health_services.msg_missing_health_service_seq')
            )
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('number'):
                values['number'] = config.health_service_sequence.get()
        return super(HealthService, cls).create(vlist)

    @fields.depends('patient', 'invoice_to')
    def on_change_patient(self, name=None):
        if self.patient and self.patient.party.insurance:
            self.invoice_to = self.patient.party.insurance[0].company.id
            self.insurance_plan = self.patient.party.insurance[0].id

    @classmethod
    def get_paid_amount(cls, records, names):
        result = {n: {s.id: Decimal(0) for s in records} for n in names}
        for name in names:
            for rec in records:
                for payment in rec.payments:
                    result[name][rec.id] += payment.amount
                # result[name][rec.id] += sale.get_total_vouchers_amount()
        return result

    def get_context_price(self, product, qty):
        pool = Pool()
        Product = pool.get('product.product')
        ctx = {
            'company': self.institution.company.id,
            'sale_date': self.service_date,
            'uom': self.product.sale_uom.id,
        }
        if self.insurance_plan:
            ctx['price_list'] = self.insurance_plan.price_list.id

        with Transaction().set_context(ctx):
            unit_price = Product.get_sale_price([product],
                qty or 0)[product.id]
            if unit_price:
                unit_price = round_price(unit_price)
            return unit_price


class HealthServiceLine(ModelSQL, ModelView):
    'Health Service Line'
    __name__ = 'health.service.line'
    service = fields.Many2One('health.service', 'Service', readonly=True)
    desc = fields.Char('Description', required=True)
    to_invoice = fields.Boolean('Invoice')
    product = fields.Many2One('product.product', 'Product', required=True)
    ward = fields.Many2One('health.hospital.ward', 'Ward')
    qty = fields.Integer('Qty')
    procedure = fields.Many2One('health.procedure', 'Procedure')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
        states={
            'readonly': True,
        })
    unit_price = fields.Numeric('Unit Price', digits=(16, 4))
    amount = fields.Function(fields.Numeric('Unit Price', digits=(16, 4)),
            'get_amount')
    from_date = fields.Date('From')
    to_date = fields.Date('To')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('invoiced', 'Invoiced'),
        ], 'State', readonly=True
    )
    origin = fields.Reference('Origin', selection='get_origin',
        states={'readonly': True})

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.appointment']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @staticmethod
    def default_qty():
        return 1

    @staticmethod
    def default_state():
        return 'draft'

    def get_amount(self, name):
        res = 0
        if self.qty and self.unit_price:
            res = Decimal(self.qty) * self.unit_price
        return res

    @classmethod
    def validate(cls, services):
        super(HealthServiceLine, cls).validate(services)
        for service in services:
            service.validate_invoice_status()

    def validate_invoice_status(self):
        if self.service and self.service.state == 'invoiced':
            raise UserError(gettext(
                'heatlh_services.msg_no_service_invoiced'
            ))

    @classmethod
    def compute_price(cls, product, insurance_plan=None):
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        template = product.template
        unit_price = template.list_price
        if insurance_plan:
            price_list = insurance_plan.price_list
            ctx = {
                'price_list': price_list.id,
                'currency': company.currency.id,
            }
            uom = template.default_uom
            qty = 1
            with Transaction().set_context(ctx):
                unit_price = price_list.compute(
                    None, product, unit_price, qty, uom
                )
        return unit_price

    @fields.depends('product', 'service', 'desc')
    def on_change_product(self):
        if not self.product or not self.service:
            return

        if self.product:
            self.desc = self.product.rec_name
        if self.product.procedure:
            self.procedure = self.product.procedure

        if self.service.insurance_plan:
            self.unit_price = HealthServiceLine.compute_price(
                self.product, self.service.insurance_plan
            )
        else:
            self.unit_price = self.product.template.list_price


class PatientPrescriptionOrder(ModelSQL, ModelView):
    'Prescription Order'
    __name__ = 'health.prescription.order'

    service = fields.Many2One('health.service', 'Service', depends=['patient'],
        domain=[('patient', '=', Eval('patient'))],
        states={'readonly': Equal(Eval('state'), 'done')},
        help="Service document associated to this prescription")

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        cls._buttons.update({
            'update_service': {
                'readonly': Equal(Eval('state'), 'done'),
            },
        })

    @classmethod
    @ModelView.button
    def update_service(cls, prescriptions):
        HealthService = Pool().get('health.service')
        prescription = prescriptions[0]

        if not prescription.service:
            raise UserError(gettext(
                'heatlh_services.msg_need_associated_service'
            ))

        service_data = {}
        service_lines = []

        # Add the prescription lines to the service document
        for line in prescription.prescription_line:
            service_lines.append(('create', [{
                'product': line.medicament.product.id,
                'desc': 'Prescription Line',
                'qty': line.quantity
            }]))

        description = "Services including " + prescription.prescription_id
        service_data['desc'] = description
        service_data['lines'] = service_lines
        HealthService.write([prescription.service], service_data)


class ServicePaymentForm(ModelView):
    'Service Payment Form'
    __name__ = 'health_services.payment_form.start'
    statement = fields.Many2One('account.statement', 'Statement',
        required=True, domain=['OR', [
            ('create_uid.login', '=', Eval('user')),
            ('state', '=', 'draft')
        ], [
            Eval('user') == 'admin',
            ('state', '=', 'draft'),
        ]])
    amount_to_pay = fields.Numeric('Amount to Pay', required=True,
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    currency_digits = fields.Integer('Currency Digits')
    voucher = fields.Char('Voucher', states={
        'required': Bool(Eval('require_voucher')),
    }, depends=['require_voucher'])
    party = fields.Many2One('party.party', 'Party')
    user = fields.Many2One('res.user', 'User')
    require_voucher = fields.Boolean('Require Voucher',
        depends=['statement'])

    @classmethod
    def default_user(cls):
        user = Pool.get('user.res')(Transaction().user)
        return user.login

    @fields.depends('statement', 'require_voucher')
    def on_change_with_require_voucher(self):
        if self.statement:
            return self.statement.journal.require_voucher
        return False

    @fields.depends('statement')
    def on_change_with_amount_to_pay(self):
        active_id = Transaction().context.get('active_id')
        Service = Pool().get('health.service')
        service = Service(active_id)
        if self.statement:
            return service.balance_payable


class WizardServicePayment(Wizard):
    'Wizard Service Payment'
    __name__ = 'health_services.payment_form'
    start = StateView('health_services.payment_form.start',
        'health_services.service_payment_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Pay', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()
    print_ = StateReport('health_services.payment_receipt_service_report')

    def do_print_(self, action):
        active_id = Transaction().context.get('active_id')
        Service = Pool().get('health.service')
        service = Service(active_id)
        data = {
            'records_ids': [service.id],
        }
        return action, data

    def transition_print_(self):
        return 'end'

    def get_amount_to_pay(self, service):
        return service.balance_payable

    def default_start(self, fields):
        pool = Pool()
        Service = pool.get('health.service')
        service = Service(Transaction().context['active_id'])
        return {
            'currency_digits': 2,
            'party': service.patient.party.id,
        }

    def update_service(self, service):
        if service.paid_amount == service.total_amount:
            service.state = 'paid'
            service.save()

    def get_line(self, form, account, service):
        StatementLine = Pool().get('account.statement.line')
        line = StatementLine(
            statement=form.statement.id,
            date=date.today(),
            amount=form.amount_to_pay,
            party=form.party.id,
            account=account,
            description=service.number,
            health_service=service.id,
            number=form.voucher,
            voucher=form.voucher,
        )
        return line

    def transition_pay_(self):
        pool = Pool()
        Service = pool.get('health.service')
        service_id = Transaction().context.get('active_id', False)
        service = Service(service_id)

        form = self.start
        try:
            account = form.party.account_receivable.id
        except:
            raise UserError(gettext(
                'health_services.party_without_account_receivable'))

        # CreateInvoice = wizard_health_services.CreateServiceInvoice()??

        if form.amount_to_pay:
            line = self.get_line(form, account, service)
            line.save()
            line.create_move()

        service.save()
        self.update_service(service)
        return 'print_'


class PaymentReceipt(Report):
    'Payment Receipt'
    __name__ = 'health_services.payment_receipt_service_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Service = pool.get('health.service')
        user = pool.get('res.user')(Transaction().user)
        time_now = datetime.now() - timedelta(hours=5)
        report_context['hour'] = time_now
        report_context['company'] = user.company
        report_context['user'] = user
        report_context['records'] = Service.browse(data['records_ids'])
        return report_context


class Appointment(metaclass=PoolMeta):
    __name__ = 'health.appointment'
    health_service = fields.Many2One('health.service', 'Health Service',
        help='Health service to invoice.', states={'readonly': True})

    @classmethod
    def check_in(cls, records):
        super(Appointment, cls).check_in(records)
        for rec in records:
            if rec.health_service:
                continue
            rec.create_service()

    def _get_line(self, product, qty):
        return {
            'desc': product.template.name,
            'to_invoice': True,
            'product': product.id,
            'unit_price': product.list_price,
            'qty': 1,
        }

    def get_service_data(self):
        data = {
            'patient': self.patient.id,
            'institution': self.institution.id,
            'service_date': date.today(),
            'state': 'draft',
            'desc': 'Appointment',
            'origin': str(self),
            'lines': [('create', [self._get_line(self.product, 1)])],
        }
        return data

    def create_service(self):
        Service = Pool().get('health.service')
        data = self.get_service_data()
        service, = Service.create([data])
        self.health_service = service.id
        self.save()
