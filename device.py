# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval


class PaymentDevice(ModelSQL, ModelView):
    'Payment Device'
    __name__ = 'payment.device'
    name = fields.Char('Device Name', required=True, select=True)
    code = fields.Char('Code', select=True)
    institution = fields.Many2One('health.institution', 'Institution')
    journals = fields.Many2Many('payment.device.account.statement.journal',
        'device', 'journal', 'Journals'
    )
    journal = fields.Many2One('account.statement.journal', "Default Journal",
        ondelete='RESTRICT', depends=['journals'],
        domain=[('id', 'in', Eval('journals', []))],
    )
    # users = fields.One2Many('res.user', 'sale_device', 'Users')


class PaymentDeviceStatementJournal(ModelSQL):
    'Payment Device - Statement Journal'
    __name__ = 'payment.device.account.statement.journal'
    _table = 'payment_device_account_statement_journal'
    device = fields.Many2One('payment.device', 'Payment Device',
            ondelete='CASCADE', select=True, required=True)
    journal = fields.Many2One('account.statement.journal', 'Statement Journal',
            ondelete='RESTRICT', required=True)
